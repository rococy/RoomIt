module org.rococy.roomit {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires com.google.gson;
    requires com.sun.jna;
    requires com.sun.jna.platform;
    requires java.sql;

    opens org.rococy.roomit.domain to com.google.gson;
    opens org.rococy.roomit to javafx.fxml;
    opens org.rococy.roomit.controller to javafx.fxml;
    exports org.rococy.roomit.control to javafx.fxml;
    exports org.rococy.roomit;
    exports org.rococy.roomit.controller;
    exports org.rococy.roomit.function;
}