package org.rococy.roomit.symbol;

import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.symbol.base.BaseRectangleSymbol;

/**
 * 画笔logo
 *
 * @author Rococy
 * @date 2022/9/11
 */
public class BrushSymbol extends BaseRectangleSymbol {

    public BrushSymbol() {
        // 宽高
        this.setWidth(4);
        this.setHeight(12);

        // 圆角
        this.setArcWidth(3);
        this.setArcHeight(3);

        // 旋转角度
        this.setRotate(-30);

        this.setStyle("-fx-fill: " + GlobalConsts.THEME_COLOR);
    }


    @Override
    public void position(double x, double y) {
        this.setX(x);
        this.setY(y);
    }
}
