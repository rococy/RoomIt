package org.rococy.roomit.symbol;

import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.symbol.base.BaseRectangleSymbol;

/**
 * 矩形logo
 *
 * @author Rococy
 * @date 2022/9/11
 */
public class RectangleSymbol extends BaseRectangleSymbol {

    public RectangleSymbol() {
        // 宽高
        this.setWidth(10);
        this.setHeight(10);

        // 设置圆角
        this.setArcWidth(5);
        this.setArcHeight(5);

        this.setStyle("-fx-fill: " + GlobalConsts.THEME_COLOR);
    }

    @Override
    public void position(double x, double y) {
        this.setX(x);
        this.setY(y);
    }
}
