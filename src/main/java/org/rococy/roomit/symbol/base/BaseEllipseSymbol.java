package org.rococy.roomit.symbol.base;

import javafx.scene.shape.Ellipse;

/**
 * @author Rococy
 * @date 2022/9/11
 */
public abstract class BaseEllipseSymbol extends Ellipse implements MouseSymbol {

    public BaseEllipseSymbol() {
        this.setViewOrder(-1);
    }

    @Override
    public BaseEllipseSymbol fillColor(String hexColor) {
        this.setStyle("-fx-fill: " + hexColor);
        return this;
    }
}
