package org.rococy.roomit.symbol.base;

/**
 * 鼠标标志
 *
 * @author Rococy
 * @date 2022/9/11
 */
public interface MouseSymbol {

    /**
     * 位置坐标
     *
     * @param x x坐标
     * @param y y坐标
     */
    void position(double x, double y);

    /**
     * 设置颜色
     *
     * @param hexColor 16进制颜色
     * @return this
     */
    MouseSymbol fillColor(String hexColor);
}
