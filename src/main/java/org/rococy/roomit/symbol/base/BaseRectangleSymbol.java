package org.rococy.roomit.symbol.base;

import javafx.scene.shape.Rectangle;

/**
 * @author Rococy
 * @date 2022/9/11
 */
public abstract class BaseRectangleSymbol extends Rectangle implements MouseSymbol {

    public BaseRectangleSymbol() {
        this.setViewOrder(-1);
    }

    @Override
    public BaseRectangleSymbol fillColor(String hexColor) {
        this.setStyle("-fx-fill: " + hexColor);
        return this;
    }
}
