package org.rococy.roomit.symbol;

import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.symbol.base.BaseEllipseSymbol;

/**
 * 椭圆logo
 *
 * @author Rococy
 * @date 2022/9/11
 */
public class EllipseSymbol extends BaseEllipseSymbol {

    public EllipseSymbol() {
        // 设置圆角
        this.setRadiusX(6);
        this.setRadiusY(6);

        this.setStyle("-fx-fill: " + GlobalConsts.THEME_COLOR);
    }

    @Override
    public void position(double x, double y) {
        this.setCenterX(x);
        this.setCenterY(y);
    }
}
