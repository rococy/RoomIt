package org.rococy.roomit;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.util.ResourceUtils;

import java.io.IOException;

/**
 * 工具箱
 *
 * @author Rococy
 * @date 2022/10/6
 */
public class ToolsWindow extends Application {

    private final Stage stage = new Stage();

    private static final ToolsWindow TOOLS_WINDOW = new ToolsWindow();

    private ToolsWindow() {
    }

    public static ToolsWindow getInstance() {
        return TOOLS_WINDOW;
    }

    {
        try {
            // 加载FXML文件
            FXMLLoader fxmlLoader = new FXMLLoader(ResourceUtils.getResource("fxml/toolsWindow.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            // 透明背景
            scene.setFill(Paint.valueOf(GlobalConsts.TRANSPARENT_COLOR));
            // 设置场景
            stage.setScene(scene);
            // 设置标题
            stage.setTitle("RoomIt-工具箱");
            // 居中
            stage.centerOnScreen();
            // 只有一个x的任务栏
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            // 设置图标
            stage.getIcons().add(new Image(ResourceUtils.getResourceAsStream(GlobalConsts.LOGO_PATH)));
            // 置顶
            stage.setAlwaysOnTop(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage stage) throws IOException {
        if (stage.isShowing()) {
            return;
        }
        // 将窗口显示出来
        stage.show();
    }

    public void show() {
        try {
            start(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isShowing() {
        return stage.isShowing();
    }

    public void setOnHidden(EventHandler<WindowEvent> value) {
        stage.setOnHidden(value);
    }

    public void hide() {
        stage.hide();
    }

}
