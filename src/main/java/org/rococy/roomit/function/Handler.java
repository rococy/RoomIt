package org.rococy.roomit.function;

/**
 * 无参数无返回值的函数式接口
 *
 * @author Rococy
 * @date 2022/9/23
 */
@FunctionalInterface
public interface Handler {

    /**
     * 使用
     */
    void apply();

}
