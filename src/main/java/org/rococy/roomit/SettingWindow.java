package org.rococy.roomit;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.util.ResourceUtils;

import java.io.IOException;

/**
 * @author Rococy
 * @date 2022/9/26
 */
public class SettingWindow extends Application {

    private final Stage stage = new Stage();

    {
        // 设置标题
        stage.setTitle("RoomIt-设置窗口");
        // 居中
        stage.centerOnScreen();
        // 只有一个x的任务栏
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        // 置顶
        stage.setAlwaysOnTop(true);
    }

    @Override
    public void start(Stage stage) throws IOException {
        // 加载FXML文件
        FXMLLoader fxmlLoader = new FXMLLoader(ResourceUtils.getResource("fxml/settingWindow.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        // 场景填充透明背景
        scene.setFill(Paint.valueOf(GlobalConsts.TRANSPARENT_COLOR));
        // 设置场景
        stage.setScene(scene);
        // 设置图标
        stage.getIcons().add(new Image(ResourceUtils.getResourceAsStream(GlobalConsts.LOGO_PATH)));
        // 将窗口显示出来
        stage.show();
    }

    public void show() {
        try {
            start(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
