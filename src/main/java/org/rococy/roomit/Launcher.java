package org.rococy.roomit;

import javafx.application.Platform;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.constant.KeyBoardConsts;
import org.rococy.roomit.domain.KeyBoardConfiguration;
import org.rococy.roomit.listener.TrayIconMouseListener;
import org.rococy.roomit.listener.WindowGlobalKeyBoardListener;
import org.rococy.roomit.util.AutoStartFile;
import org.rococy.roomit.util.KeyBoardUtils;
import org.rococy.roomit.util.ResourceUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

/**
 * @author Rococy
 * @date 2022/10/12
 */
public class Launcher {

    public static void main(String[] args) throws IOException, AWTException {
        new Launcher().launch();
    }

    private void launch() throws IOException, AWTException {
        initPreConfig();
        initTrayIcon();
        initGlobalKeyBoardListener();
    }

    private void initPreConfig() {
        // 加载自启动文件
        AutoStartFile.toggleExists();
        // 设置当程序所有窗口都关闭时，JavaFx的UI线程继续运行
        Platform.setImplicitExit(false);
        // 初始化Toolkit
        Platform.startup(() -> {
        });
    }

    private void initTrayIcon() throws IOException, AWTException {
        // 获取系统托盘
        SystemTray systemTray = SystemTray.getSystemTray();
        // 加载托盘图片 16*16的png图片
        TrayIcon trayIcon = new TrayIcon(ImageIO.read(ResourceUtils.getResource(GlobalConsts.LOGO_PATH)), "RoomIt");
        // 托盘图片自适应大小
        trayIcon.setImageAutoSize(true);
        trayIcon.addMouseListener(new TrayIconMouseListener());
        systemTray.add(trayIcon);
    }

    private void initGlobalKeyBoardListener() {
        // 设置全局快捷键
        KeyBoardConfiguration keyBoardConfiguration = ConfigurationManager.getKeyboardConfiguration();
        WindowGlobalKeyBoardListener.addEventListener(event -> {
            String keyBoardText = KeyBoardUtils.parse(event);
            String type = keyBoardConfiguration.getType(keyBoardText);
            // 关闭或启动窗口
            if (type.equals(KeyBoardConsts.OPEN_OR_CLOSE)) {
                Platform.runLater(MainWindow::toggleDisplay);
            }
        });
    }
}
