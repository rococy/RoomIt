package org.rococy.roomit.listener;

import javafx.application.Platform;
import org.rococy.roomit.MainWindow;
import org.rococy.roomit.TrayWindow;
import org.rococy.roomit.adapter.MouseListenerAdapter;

import java.awt.event.MouseEvent;

/**
 * @author Rococy
 * @date 2022/9/25
 */
public class TrayIconMouseListener extends MouseListenerAdapter {

    private static final int MOUSE_LEFT_BUTTON = 1, MOUSE_RIGHT_BUTTON = 3;

    @Override
    public void mousePressed(MouseEvent e) {
        switch (e.getButton()) {
            // 左键
            case MOUSE_LEFT_BUTTON -> Platform.runLater(MainWindow::toggleDisplay);

            // 右键
            case MOUSE_RIGHT_BUTTON -> Platform.runLater(() -> TrayWindow.getInstance().show(e.getXOnScreen(), e.getYOnScreen()));
        }
    }

}
