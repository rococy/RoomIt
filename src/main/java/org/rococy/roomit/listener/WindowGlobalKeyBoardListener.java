package org.rococy.roomit.listener;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.rococy.roomit.MainWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * @author Rococy
 * @date 2022/10/3
 */
public class WindowGlobalKeyBoardListener {

    private static final List<Consumer<KeyEvent>> EVENT_LIST = new ArrayList<>(10);
    private static WinUser.HHOOK hhk = null;

    static {
        startNativeKeyBoardListener();
    }

    public static void addEventListener(Consumer<KeyEvent> event) {
        EVENT_LIST.add(event);
    }

    private static void startNativeKeyBoardListener() {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(() -> {
            final User32 lib = User32.INSTANCE;
            WinDef.HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
            GlobalKeyEvent keyEvent = new GlobalKeyEvent();
            WinUser.LowLevelKeyboardProc keyboardHook = (nCode, wParam, info) -> {
                if (nCode < 0) {
                    return lib.CallNextHookEx(hhk, nCode, wParam, new WinDef.LPARAM(Pointer.nativeValue(info.getPointer())));
                }

                int code = info.vkCode;
                switch (wParam.intValue()) {
                    // 键盘按下
                    case WinUser.WM_KEYDOWN -> {
                        switch (code) {
                            case 160 -> keyEvent.isShiftDown = true;
                            case 162 -> keyEvent.isCtrlDown = true;
                            case 164 -> keyEvent.isAltDown = true;
                            // esc
                            case 27 -> {
                                if (MainWindow.isShowing()) {
                                    Platform.runLater(MainWindow::toggleDisplay);
                                }
                            }
                        }

                        if (code >= 48 && code <= 57 || code >= 65 && code <= 90) {
                            // 字母或数字
                            keyEvent.keyCode = KeyCode.getKeyCode(String.valueOf((char) code));
                            // 触发事件
                            EVENT_LIST.forEach(c -> c.accept(new KeyEvent(null, null, null, null, null, keyEvent.keyCode, keyEvent.isShiftDown, keyEvent.isCtrlDown, keyEvent.isAltDown, false)));
                        }

                    }

                    // 键盘抬起
                    case WinUser.WM_KEYUP -> {
                        switch (code) {
                            case 160 -> keyEvent.isShiftDown = false;
                            case 162 -> keyEvent.isCtrlDown = false;
                            case 164 -> keyEvent.isAltDown = false;
                        }
                    }
                }

                return lib.CallNextHookEx(hhk, nCode, wParam, new WinDef.LPARAM(Pointer.nativeValue(info.getPointer())));
            };

            hhk = lib.SetWindowsHookEx(WinUser.WH_KEYBOARD_LL, keyboardHook, hMod, 0);
            int result;
            WinUser.MSG msg = new WinUser.MSG();
            while ((result = lib.GetMessage(msg, null, 0, 0)) != 0) {
                if (result == -1) {
                    // error in get message
                    break;
                }

                // got message
                lib.TranslateMessage(msg);
                lib.DispatchMessage(msg);
            }
            lib.UnhookWindowsHookEx(hhk);
        });
    }

    public static class GlobalKeyEvent {
        private boolean isShiftDown;
        private boolean isCtrlDown;
        private boolean isAltDown;
        private KeyCode keyCode;
    }

}
