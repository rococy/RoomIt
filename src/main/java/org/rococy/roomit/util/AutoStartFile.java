package org.rococy.roomit.util;

import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.domain.BasicConfiguration;

import java.io.*;
import java.nio.charset.Charset;
import java.util.StringJoiner;

/**
 * @author Rococy
 * @date 2022/10/12
 */
public class AutoStartFile {

    private static final BasicConfiguration BASIC_CONFIGURATION = ConfigurationManager.getBasicConfiguration();

    public static void toggleExists() {
        if (BASIC_CONFIGURATION.getAutoStart()) {
            generate();
        } else {
            delete();
        }
    }

    @SuppressWarnings("all")
    private static void generate() {
        String startUpFilePath = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\RoomIt.bat";
        String copyAutoStartFilePath = ResourceUtils.getResourceByProject("config/copy_auto_start_file.bat");
        String batFilePath = ResourceUtils.getResourceByProject("config/RoomIt.bat");
        String autoStartExecFilePath = ResourceUtils.getResourceByProject("");
        String fileContent = getAutoStartFileContent(autoStartExecFilePath);

        FileInputStream is = null;
        FileWriter fos = null;
        try {
            File startUpFile = new File(startUpFilePath);

            // 如果自启文件存在，查看其内容是否相同
            if (startUpFile.exists()) {
                is = new FileInputStream(startUpFile);
                boolean isSame = new String(is.readAllBytes()).equals(fileContent);

                if (isSame) {
                    return;
                }
            }

            // 复制自启动bat文件到windows的startup目录下
            File file = new File(batFilePath);

            if (!file.exists()) {
                file.createNewFile();
            }

            // windows平台使用GBK字符集
            fos = new FileWriter(file, Charset.forName("GBK"));
            fos.write(fileContent);
            fos.flush();

            executeBatFile(copyAutoStartFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void delete() {
        executeBatFile(ResourceUtils.getResourceByProject("config/cancel_auto_start.bat"));
    }


    private static String getAutoStartFileContent(String autoStartExecFilePath) {
        // 自启文件内容
        StringJoiner fileContentJoiner = new StringJoiner("\r\n");
        String driveLetter = autoStartExecFilePath.substring(0, 2);
        fileContentJoiner.add("@echo off");
        fileContentJoiner.add("if exist " + autoStartExecFilePath + "RoomIt.exe (");
        fileContentJoiner.add(driveLetter);
        fileContentJoiner.add("cd " + autoStartExecFilePath);
        fileContentJoiner.add("start /b RoomIt");
        fileContentJoiner.add(") else (");
        fileContentJoiner.add("exit");
        fileContentJoiner.add(")");
        return fileContentJoiner.toString();
    }

    /**
     * 执行bat文件
     *
     * @param file bat文件路径
     * @return bat文件输出log
     */
    private static String executeBatFile(String file) {
        String cmdCommand = "cmd.exe /c " + file;
        StringBuilder stringBuilder = new StringBuilder();
        Process process;
        try {
            process = Runtime.getRuntime().exec(cmdCommand);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream(), "GBK"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
