package org.rococy.roomit.util;

import org.rococy.roomit.MainWindow;

import java.io.InputStream;
import java.net.URL;

/**
 * @author Rococy
 * @date 2022/10/9
 */
public class ResourceUtils {

    private static final Class<MainWindow> MAIN_WINDOW_CLASS = MainWindow.class;

    private static final String USER_DIR = System.getProperty("user.dir").replaceAll("\\\\", "/");

    private ResourceUtils() {
    }

    public static URL getResource(String name) {
        return MAIN_WINDOW_CLASS.getResource(name);
    }

    public static InputStream getResourceAsStream(String name) {
        return MAIN_WINDOW_CLASS.getResourceAsStream(name);
    }

    public static String getResourceByProject(String name) {
        return USER_DIR.concat("/").concat(name);
    }
}
