package org.rococy.roomit.util;

import java.awt.Toolkit;

/**
 * @author Rococy
 * @date 2022/9/10
 */
public final class ScreenUtils {

    /**
     * 工具箱
     */
    private static final Toolkit TOOLKIT = Toolkit.getDefaultToolkit();

    /**
     * 获取屏幕宽高
     */
    private static final double SCREEN_WIDTH = TOOLKIT.getScreenSize().getWidth();
    private static final double SCREEN_HEIGHT = TOOLKIT.getScreenSize().getHeight();

    private ScreenUtils() {
    }

    /**
     * @return 屏幕宽
     */
    public static double getScreenWidth() {
        return SCREEN_WIDTH;
    }

    /**
     * @return 屏幕高
     */
    public static double getScreenHeight() {
        return SCREEN_HEIGHT;
    }

}
