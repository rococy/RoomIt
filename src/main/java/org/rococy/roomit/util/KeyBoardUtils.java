package org.rococy.roomit.util;

import javafx.scene.input.KeyEvent;

import java.util.StringJoiner;

/**
 * @author Rococy
 * @date 2022/10/4
 */
public class KeyBoardUtils {

    public static String parse(KeyEvent e) {
        StringJoiner keyBoardJoiner = new StringJoiner("+");
        String keyBoard = "";
        if (e.isControlDown()) {
            keyBoardJoiner.add("CTRL");
        }

        if (e.isShiftDown()) {
            keyBoardJoiner.add("SHIFT");
        }

        if (e.isAltDown()) {
            keyBoardJoiner.add("ALT");
        }

        // 如果上面的键一个没点
        if (keyBoardJoiner.length() == 0) {
            return keyBoard;
        }

        keyBoard = keyBoardJoiner.add(e.getCode().getChar()).toString();
        return keyBoard;
    }

}
