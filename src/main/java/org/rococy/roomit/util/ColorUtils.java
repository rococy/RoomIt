package org.rococy.roomit.util;

import javafx.scene.paint.Color;

/**
 * @author Rococy
 * @date 2022/9/19
 */
public class ColorUtils {


    private ColorUtils() {
    }


    /**
     * 转为16进制颜色字符串
     *
     * @param color color object
     * @return 16进制颜色字符串
     */
    public static String toHexStr(Color color) {
        return color.toString().replaceFirst("0x", "#");
    }

}
