package org.rococy.roomit.util;

import javafx.scene.Node;

import java.util.HashMap;
import java.util.Map;

/**
 * 处理控件的style的修改类
 *
 * @author Rococy
 * @date 2022/9/20
 */
public class StyleRenderer {

    /**
     * 控件
     */
    private final Node styleNode;

    /**
     * 字符串拼接器
     */
    private final StringBuilder sb = new StringBuilder();

    /**
     * HashMap
     */
    private final Map<String, Object> map = new HashMap<>(10);

    /**
     * @param styleNode 可以设置样式的控件
     */
    public StyleRenderer(Node styleNode) {
        this.styleNode = styleNode;
    }

    /**
     * 给控件添加样式
     *
     * @param prop 属性
     * @param val  值
     */
    public StyleRenderer add(String prop, Object val) {
        if (!(val instanceof String)) {
            val = val.toString() + "px";
        }
        map.put(prop, val);
        return this;
    }

    /**
     * 修改控件的样式
     */
    public void render() {
        // 拼接样式
        map.forEach((key, value) -> sb.append(key).append(":").append(value).append(";"));

        // 设置样式
        styleNode.setStyle(sb.toString());

        // 清空拼接器
        sb.setLength(0);
    }
}