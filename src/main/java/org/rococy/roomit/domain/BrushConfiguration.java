package org.rococy.roomit.domain;

/**
 * @author Rococy
 * @date 2022/9/29
 */
public class BrushConfiguration {

    private boolean transparent;

    private String strokeColor;

    private double lineWidth;

    public boolean getTransparent() {
        return transparent;
    }

    public void setTransparent(boolean transparent) {
        this.transparent = transparent;
    }

    public String getStrokeColor() {
        return strokeColor;
    }

    public void setStrokeColor(String strokeColor) {
        this.strokeColor = strokeColor;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }
}
