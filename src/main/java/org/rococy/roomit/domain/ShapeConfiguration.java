package org.rococy.roomit.domain;

/**
 * @author Rococy
 * @date 2022/9/29
 */
public class ShapeConfiguration {

    private BasicConfiguration basic;
    private BrushConfiguration brush;
    private RectangleConfiguration rectangle;
    private EllipseConfiguration ellipse;

    public BasicConfiguration getBasic() {
        return basic;
    }

    public void setBasic(BasicConfiguration basic) {
        this.basic = basic;
    }

    public BrushConfiguration getBrush() {
        return brush;
    }

    public void setBrush(BrushConfiguration brush) {
        this.brush = brush;
    }

    public RectangleConfiguration getRectangle() {
        return rectangle;
    }

    public void setRectangle(RectangleConfiguration rectangle) {
        this.rectangle = rectangle;
    }

    public EllipseConfiguration getEllipse() {
        return ellipse;
    }

    public void setEllipse(EllipseConfiguration ellipse) {
        this.ellipse = ellipse;
    }
}
