package org.rococy.roomit.domain;

/**
 * @author Rococy
 * @date 2022/9/29
 */
public class RectangleConfiguration {

    private String borderColor;

    private double borderWidth;

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public double getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(double borderWidth) {
        this.borderWidth = borderWidth;
    }
}
