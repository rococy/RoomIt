package org.rococy.roomit.domain;

/**
 * 记录canvas画的每一个点
 *
 * @author Rococy
 * @date 2022/9/24
 */
public class CanvasPoint {

    public final double startX, startY, endX, endY;
    public final String strokeColor;

    public CanvasPoint(double startX, double startY, double endX, double endY, String strokeColor) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.strokeColor = strokeColor;
    }

}
