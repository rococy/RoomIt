package org.rococy.roomit.domain;

/**
 * @author Rococy
 * @date 2022/10/12
 */
public class BasicConfiguration {

    private Boolean autoStart;
    private Double drawingBoardOpacity;

    public Double getDrawingBoardOpacity() {
        return drawingBoardOpacity == 0.0 ? 0.05 : drawingBoardOpacity;
    }

    public void setDrawingBoardOpacity(Double drawingBoardOpacity) {
        this.drawingBoardOpacity = drawingBoardOpacity;
    }

    public Boolean getAutoStart() {
        return autoStart;
    }

    public void setAutoStart(Boolean autoStart) {
        this.autoStart = autoStart;
    }
}
