package org.rococy.roomit.domain;

import java.util.Map;

/**
 * @author Rococy
 * @date 2022/10/4
 */
public class KeyBoardConfiguration {

    private final Map<String, String> keyBoardMap;

    public KeyBoardConfiguration(Map<String, String> keyBoardMap) {
        this.keyBoardMap = keyBoardMap;
    }

    public String getType(String keyBoard) {
        for (Map.Entry<String, String> entry : keyBoardMap.entrySet()) {
            if (entry.getValue().equals(keyBoard)) {
                return entry.getKey();
            }
        }
        return "";
    }

    public Map<String, String> getKeyBoardMap() {
        return keyBoardMap;
    }
}
