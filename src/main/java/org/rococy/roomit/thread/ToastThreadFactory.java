package org.rococy.roomit.thread;

import java.util.concurrent.ThreadFactory;

/**
 * @author Rococy
 * @date 2022/9/8
 */
public class ToastThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setName("Toast-Thread-%d");
        return t;
    }

}
