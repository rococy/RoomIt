package org.rococy.roomit;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.util.ResourceUtils;

import java.io.IOException;

/**
 * 托盘右键的窗口
 *
 * @author Rococy
 * @date 2022/9/25
 */
public class TrayWindow extends Application {

    private final Stage stage = new Stage();

    /**
     * 单例设计模式
     */
    private static TrayWindow trayWindow = null;
    private TrayWindow() {}
    public static TrayWindow getInstance() {
        if(trayWindow == null) {
            trayWindow = new TrayWindow();
        }
        return trayWindow;
    }

    {
        try {
            // 加载FXML文件
            FXMLLoader fxmlLoader = new FXMLLoader(ResourceUtils.getResource("fxml/trayWindow.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            // 设置场景
            stage.setScene(scene);
            // 场景填充透明背景
            scene.setFill(Paint.valueOf(GlobalConsts.TRANSPARENT_COLOR));
            // 置顶
            stage.setAlwaysOnTop(true);
            // 取消任务栏
            stage.initStyle(StageStyle.TRANSPARENT);
            // 设置图标
            stage.getIcons().add(new Image(ResourceUtils.getResourceAsStream(GlobalConsts.LOGO_PATH)));

            // 当没有焦点时关闭
            stage.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    return;
                }
                stage.close();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void start(Stage stage) {
        if (stage.isShowing()) {
            return;
        }
        stage.show();
    }

    public void show(double x, double y) {
        // Screen.getPrimary().getOutputScaleX获取屏幕缩放比
        stage.setX(x / Screen.getPrimary().getOutputScaleX() - 210);
        stage.setY(y / Screen.getPrimary().getOutputScaleY() - 75);
        start(stage);
    }
}
