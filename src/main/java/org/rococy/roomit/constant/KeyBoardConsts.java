package org.rococy.roomit.constant;

/**
 * @author Rococy
 * @date 2022/10/4
 */
public class KeyBoardConsts {

    public static final String BRUSH = "brush";
    public static final String RECTANGLE = "rectangle";
    public static final String ELLIPSE = "ellipse";
    public static final String TOOLS = "tools";
    public static final String COLOR_PICKER = "colorPicker";
    public static final String EXPEND_MOUSE = "expendMouse";
    public static final String CLEAR = "clear";
    public static final String SCREENSHOT_AND_COPY = "screenshotAndCopy";
    public static final String OPEN_OR_CLOSE = "openOrClose";
    public static final String BACK = "back";

}
