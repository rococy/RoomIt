package org.rococy.roomit.constant;

import org.rococy.roomit.util.ResourceUtils;

/**
 * @author Rococy
 * @date 2022/9/19
 */
public class GlobalConsts {

    /**
     * 主题色
     */
    public static final String THEME_COLOR = "linear-gradient(to bottom, #f64f59, #c471ed, #12c2e9)";

    /**
     * 透明色
     */
    public static final String TRANSPARENT_COLOR = "#00000000";

    /**
     * logo图标路径
     */
    public static final String LOGO_PATH = "img/logo24x24.png";

    /**
     * 配置文件路径
     */
    public static final String CONFIGURATION_FILE_PATH = ResourceUtils.getResourceByProject("config/config.json");

}
