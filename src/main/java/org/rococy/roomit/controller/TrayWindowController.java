package org.rococy.roomit.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import org.rococy.roomit.SettingWindow;

/**
 * @author Rococy
 * @date 2022/9/25
 */
public class TrayWindowController {

    @FXML
    private VBox container;

    private SettingWindow settingWindow;

    @FXML
    public void openSettingWindow() {
        // 关闭当前窗口
        container.getScene().getWindow().hide();
        // 打开设置窗口
        if (settingWindow == null) {
            settingWindow = new SettingWindow();
        }
        settingWindow.show();
    }

    @FXML
    public void systemExit() {
        // 关闭JavaFx线程
        Platform.exit();
        // 全局关闭
        System.exit(0);
    }

}
