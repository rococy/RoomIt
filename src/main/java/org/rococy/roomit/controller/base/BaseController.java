package org.rococy.roomit.controller.base;

import org.rococy.roomit.controller.context.ControllerContext;

/**
 * @author Rococy
 * @date 2022/10/7
 */
public abstract class BaseController {

    public BaseController() {
        ControllerContext.addController(this);
    }

}
