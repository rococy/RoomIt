package org.rococy.roomit.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import org.rococy.roomit.ToolsWindow;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.KeyBoardConsts;
import org.rococy.roomit.control.Container;
import org.rococy.roomit.control.*;
import org.rococy.roomit.controller.base.BaseController;
import org.rococy.roomit.control.RoomItCursor;
import org.rococy.roomit.domain.KeyBoardConfiguration;
import org.rococy.roomit.event.RoomItCursorEvent;
import org.rococy.roomit.function.Handler;
import org.rococy.roomit.symbol.BrushSymbol;
import org.rococy.roomit.symbol.EllipseSymbol;
import org.rococy.roomit.symbol.RectangleSymbol;
import org.rococy.roomit.symbol.base.MouseSymbol;
import org.rococy.roomit.util.ColorUtils;
import org.rococy.roomit.util.ImageTransfer;
import org.rococy.roomit.util.KeyBoardUtils;
import org.rococy.roomit.util.ScreenUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;


/**
 * @author Rococy
 * @date 2022/9/8
 */
public class MainWindowController extends BaseController implements Initializable {

    @FXML
    private Container container;

    /**
     * 自定义鼠标
     */
    private final RoomItCursor cursor = new RoomItCursor();

    /**
     * 画笔logo
     */
    private static final BrushSymbol BRUSH_SYMBOL = new BrushSymbol();

    /**
     * 矩形logo
     */
    private static RectangleSymbol rectangleSymbol;

    /**
     * 圆形logo
     */
    private static EllipseSymbol ellipseSymbol;

    /**
     * 装载鼠标logo的临时变量，默认为画笔
     */
    private Shape mouseLogo = BRUSH_SYMBOL;

    /**
     * 全局x，y轴
     */
    private double globalMouseX, globalMouseY;

    /**
     * 画线的画板
     */
    private final CanvasGroup canvasGroup = new CanvasGroup();

    /**
     * 画矩形、圆形的画板
     */
    private final ShapePane shapePane = new ShapePane();

    /**
     * 颜色选择框
     */
    private final ColorPicker colorPicker = new ColorPicker();

    /**
     * 文件配置
     */
    private final KeyBoardConfiguration keyBoardConfiguration = ConfigurationManager.getKeyboardConfiguration();

    @FXML
    public void windowKeyPressed(KeyEvent event) {
        this.triggerKeyboard(KeyBoardUtils.parse(event));
    }

    @FXML
    public void windowMouseEntered() {
        container.requestFocus();
    }

    /**
     * 初始化控件
     *
     * @param location  本地链接
     * @param resources 资源
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // 初始化最外层的pane
        initPaneWrapper();
        // 初始化自定义鼠标
        initCursor();
        // 初始化ColorPicker
        initColorPicker();
        // 初始化两个画板
        initDrawingBoard();
    }

    /**
     * 触发用户定义的快捷键
     *
     * @param keyBoardText 经过转换后的快捷键
     */
    public void triggerKeyboard(String keyBoardText) {
        switch (keyBoardConfiguration.getType(keyBoardText)) {
            // 切换为画笔
            case KeyBoardConsts.BRUSH -> changeState(BRUSH_SYMBOL);

            // 切换为绘制矩形状态
            case KeyBoardConsts.RECTANGLE -> {
                if (rectangleSymbol == null) {
                    rectangleSymbol = new RectangleSymbol();
                }
                changeState(rectangleSymbol);
                shapePane.switchToDrawRectangle();
            }

            // 切换为绘制椭圆状态
            case KeyBoardConsts.ELLIPSE -> {
                if (ellipseSymbol == null) {
                    ellipseSymbol = new EllipseSymbol();
                }
                changeState(ellipseSymbol);
                shapePane.switchToDrawEllipse();
            }

            case KeyBoardConsts.TOOLS -> ejectToolsWindow();

            // 弹出颜色框 picker
            case KeyBoardConsts.COLOR_PICKER -> ejectColorPicker();

            // 鼠标放大操作 expand
            case KeyBoardConsts.EXPEND_MOUSE -> cursor.expand(globalMouseX, globalMouseY);

            // 清空操作 clear
            case KeyBoardConsts.CLEAR -> this.clear();

            // 回退操作
            case KeyBoardConsts.BACK -> canvasGroup.undo();

            // 将截图放入剪切板操作
            case KeyBoardConsts.SCREENSHOT_AND_COPY -> {
                ToolsWindow toolsWindow = ToolsWindow.getInstance();
                if (toolsWindow.isShowing()) {
                    toolsWindow.setOnHidden(e -> {
                        try {
                            Thread.sleep(500);
                            this.captureToClipBoard();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    });
                    toolsWindow.hide();
                } else {
                    this.captureToClipBoard();
                }
            }

            case KeyBoardConsts.OPEN_OR_CLOSE -> container.getScene().getWindow().hide();
        }
    }

    /**
     * 初始化最外层的pane
     */
    private void initPaneWrapper() {
        // 设置透明度
        container.setStyle("-fx-background-color: rgba(0,0,0," + ConfigurationManager.getBasicConfiguration().getDrawingBoardOpacity() + ")");
        // 设置宽高
        container.setPrefSize(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenWidth());
        // 弹出提示消息
        container.addChildren(new Toast("屏幕工具已开启"));
        // 绑定事件
        EventHandler<MouseEvent> paneMouseEventHandler = (e) -> {
            // 记录全局x、y
            globalMouseX = e.getX();
            globalMouseY = e.getY();

            cursor.relocate(globalMouseX, globalMouseY);
        };
        container.addEventHandler(MouseEvent.MOUSE_PRESSED, paneMouseEventHandler);
        container.addEventHandler(MouseEvent.MOUSE_DRAGGED, paneMouseEventHandler);
        container.addEventHandler(MouseEvent.MOUSE_ENTERED, paneMouseEventHandler);
        container.addEventHandler(MouseEvent.MOUSE_MOVED, paneMouseEventHandler);
    }

    /**
     * 初始化两个画板
     */
    private void initDrawingBoard() {
        container.addChildren(canvasGroup, shapePane);
        canvasGroup.toFront();
        shapePane.toBack();
    }

    /**
     * 初始化颜色选择框
     */
    private void initColorPicker() {
        colorPicker.getStyleClass().add("button");
        colorPicker.toFront();

        colorPicker.setOnAction(e -> {
            // 获取16进制颜色
            String hexColor = ColorUtils.toHexStr(colorPicker.getValue());
            // 更改鼠标logo颜色
            ((MouseSymbol) mouseLogo).fillColor(hexColor);

            this.handleMouseChange(() -> canvasGroup.getChildren().forEach(c -> ((RoomItCanvas) c).setStrokeColor(hexColor)),
                    () -> shapePane.setRectangleBorderColor(hexColor),
                    () -> shapePane.setEllipseBorderColor(hexColor));
        });

        container.addChildren(colorPicker);
    }

    /**
     * 初始化自定义鼠标
     */
    private void initCursor() {
        container.addEventHandler(RoomItCursorEvent.HIDE, e -> {
            cursor.setOpacity(0);
            mouseLogo.setOpacity(0);
        });
        container.addEventHandler(RoomItCursorEvent.SHOW, e -> {
            cursor.setOpacity(1);
            mouseLogo.setOpacity(1);
        });

        // 将默认鼠标隐藏，使用自定义鼠标
        container.setCursor(Cursor.NONE);

        cursor.setCursorLogo((MouseSymbol) mouseLogo);

        container.addChildren(mouseLogo, cursor);
    }

    /**
     * 切换两个画板在屏幕的层叠顺序
     */
    private void switchPane() {
        Handler shapeHandler = () -> {
            shapePane.toFront();
            canvasGroup.toBack();
        };

        this.handleMouseChange(() -> {
            shapePane.toBack();
            canvasGroup.toFront();
        }, shapeHandler, shapeHandler);

    }

    /**
     * 切换绘画状态
     */
    private void changeState(Shape mouseSymbol) {
        changeMouseLogo(mouseSymbol);
        switchPane();
    }

    /**
     * 更改鼠标logo
     */
    private void changeMouseLogo(Shape mouseSymbol) {
        // 删除旧的
        container.removeChild(mouseLogo);

        // 更换logo
        mouseLogo = mouseSymbol;
        cursor.setCursorLogo((MouseSymbol) mouseSymbol);

        // 创建新的 并展示
        container.addChildren(mouseLogo);
    }

    private void handleMouseChange(Handler b, Handler r, Handler e) {
        if (mouseLogo instanceof BrushSymbol) {
            b.apply();
        } else if (mouseLogo instanceof RectangleSymbol) {
            r.apply();
        } else {
            e.apply();
        }
    }

    /**
     * 弹出颜色选择框
     */
    private void ejectColorPicker() {
        colorPicker.setLayoutX(globalMouseX + 10);
        colorPicker.setLayoutY(globalMouseY - 20);
        colorPicker.show();
    }

    /**
     * 弹出工具箱
     */
    private void ejectToolsWindow() {
        ToolsWindow.getInstance().show();
    }

    /**
     * 截屏并粘贴至剪切板
     */
    private void captureToClipBoard() {
        try {
            Robot robot = new Robot();
            BufferedImage image = robot.createScreenCapture(new Rectangle(0, 0, (int) ScreenUtils.getScreenWidth(), (int) ScreenUtils.getScreenHeight()));
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new ImageTransfer(image), null);

            Toast toast = new Toast("截图已粘至剪切板", () -> {
                // 将容器背景设为空
                container.setBackground(Background.EMPTY);
                // 清空画板
                this.clear();
                // 隐藏自定义鼠标
                cursor.hide();
                // 隐藏颜色选择器
                colorPicker.hide();
                // 设置回用户默认鼠标样式
                container.setCursor(Cursor.DEFAULT);
            });

            toast.setOnTransparentReleased(() -> {
                Stage stage = ((Stage) container.getScene().getWindow());
                if (stage != null) {
                    stage.close();
                }
            });

            container.addChildren(toast);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    /**
     * 清空两个画板
     */
    private void clear() {
        canvasGroup.clear();
        shapePane.clear();
        container.requestFocus();
    }

}