package org.rococy.roomit.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.constant.KeyBoardConsts;
import org.rococy.roomit.control.RoomItSlider;
import org.rococy.roomit.domain.BasicConfiguration;
import org.rococy.roomit.domain.BrushConfiguration;
import org.rococy.roomit.domain.EllipseConfiguration;
import org.rococy.roomit.domain.RectangleConfiguration;
import org.rococy.roomit.util.AutoStartFile;
import org.rococy.roomit.util.ColorUtils;

import java.awt.*;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;

/**
 * @author Rococy
 * @date 2022/9/26
 */
public class SettingWindowController {

    private final BasicConfiguration basicConfiguration = ConfigurationManager.getBasicConfiguration();
    private final BrushConfiguration brushConfiguration = ConfigurationManager.getBrushConfiguration();
    private final RectangleConfiguration rectangleConfiguration = ConfigurationManager.getRectangleConfiguration();
    private final EllipseConfiguration ellipseConfiguration = ConfigurationManager.getEllipseConfiguration();
    private final Map<String, String> keyboardConfiguration = ConfigurationManager.getKeyboardConfiguration().getKeyBoardMap();

    private static final String SLIDE_BUTTON_SELECTED_CLASS = "selected";

    /**
     * 左边导航区域
     */
    @FXML
    private Button basicBtn;
    private Button preSelectedBtn;

    /**
     * 右边内容区域
     */
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private VBox contentVbox;

    /**
     * 画笔透明单选组
     */
    @FXML
    private ToggleGroup transparentGroup;
    /**
     * 是否透明
     */
    @FXML
    private RadioButton transparentRadioBtn;
    @FXML
    private RadioButton notTransparentRadioBtn;
    @FXML
    private ColorPicker brushStrokeColorPicker;
    @FXML
    private RoomItSlider brushLineWidthSlider;

    /**
     * 矩形
     */
    @FXML
    private ColorPicker rectangleBorderColorPicker;
    @FXML
    private RoomItSlider rectangleBorderWidthSlider;

    /**
     * 椭圆
     */
    @FXML
    private ColorPicker ellipseBorderColorPicker;
    @FXML
    private RoomItSlider ellipseBorderWidthSlider;

    /**
     * 是否开机自启
     */
    @FXML
    private ToggleGroup autoStartGroup;
    @FXML
    private RadioButton autoStartBtn;
    @FXML
    private RadioButton notAutoStartBtn;

    /**
     * 画板亮度
     */
    @FXML
    private RoomItSlider drawingBoardOpacitySlider;

    /**
     * 快捷键
     */
    @FXML
    private TextField brushKeyboard;
    @FXML
    private TextField rectangleKeyboard;
    @FXML
    private TextField ellipseKeyboard;
    @FXML
    private TextField toolsKeyboard;
    @FXML
    private TextField colorPickerKeyboard;
    @FXML
    private TextField expendMouseKeyboard;
    @FXML
    private TextField clearKeyboard;
    @FXML
    private TextField screenshotAndCopyKeyboard;
    @FXML
    private TextField openOrCloseKeyboard;

    @FXML
    public void switchSideButtons(MouseEvent event) {
        Button button = (Button) event.getSource();
        List<String> classNames = button.getStyleClass();

        // 重复点击
        if (classNames.contains(SLIDE_BUTTON_SELECTED_CLASS)) {
            return;
        }

        preSelectedBtn.getStyleClass().remove("selected");
        classNames.add("selected");
        preSelectedBtn = button;

        // 移动到指定位置
        VBox child = (VBox) contentVbox.getChildren().get((int) button.getViewOrder());
        scrollPane.setVvalue(Double.parseDouble((String) child.getUserData()));
    }

    @FXML
    public void openBrowser(MouseEvent event) throws Exception {
        Hyperlink hyperlink = (Hyperlink) event.getSource();
        Desktop.getDesktop().browse(new URL((String) hyperlink.getUserData()).toURI());
    }

    @FXML
    public void keyboardTextFieldMouseClick(MouseEvent e) {
        ((TextField) e.getSource()).selectAll();
    }

    @FXML
    public void keyboardTextFieldKeyPress(KeyEvent e) {
        if (e.getText().isEmpty()) {
            return;
        }

        TextField textField = (TextField) e.getSource();
        String key = (String) textField.getUserData();
        StringJoiner keyBoardJoiner = new StringJoiner("+");

        if (e.isControlDown()) {
            keyBoardJoiner.add("CTRL");
        }

        if (e.isShiftDown()) {
            keyBoardJoiner.add("SHIFT");
        }

        if (e.isAltDown()) {
            keyBoardJoiner.add("ALT");
        }

        // 如果上面的键一个没点
        if (keyBoardJoiner.length() == 0) {
            return;
        }

        String text = keyBoardJoiner.add(e.getCode().getChar()).toString();
        textField.setText(text);
        textField.selectEnd();

        // 覆盖配置
        keyboardConfiguration.put(key, text);
    }

    @FXML
    public void initialize() {
        preSelectedBtn = basicBtn;
        initConfiguration();
        initConfigurationListener();
    }

    private void initConfiguration() {
        initBasicConfiguration();
        initBrushConfiguration();
        initRectangleConfiguration();
        initEllipseConfiguration();
        initKeyboardConfiguration();
    }

    private void initConfigurationListener() {
        // 是否开机自启
        autoStartGroup.selectedToggleProperty()
                .addListener((observable, oldValue, newValue) -> {
                    basicConfiguration.setAutoStart(Boolean.parseBoolean((String) newValue.getUserData()));
                    // 异步进行文件操作
                    CompletableFuture.runAsync(AutoStartFile::toggleExists);
                });
        // 画板亮度
        drawingBoardOpacitySlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> {
                    int val = (int) Math.round(newValue.doubleValue());
                    // 10转成0
                    double opacity = Double.parseDouble("0." + (10 - val));
                    basicConfiguration.setDrawingBoardOpacity(opacity);
                });

        // 是否透明
        transparentGroup.selectedToggleProperty()
                .addListener((observable, oldValue, newValue) -> brushConfiguration.setTransparent(Boolean.parseBoolean((String) newValue.getUserData())));
        // 线条颜色
        brushStrokeColorPicker.setOnAction(e -> {
            brushConfiguration.setStrokeColor(ColorUtils.toHexStr(brushStrokeColorPicker.getValue()));
        });
        // 线条粗细
        brushLineWidthSlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> brushConfiguration.setLineWidth(Math.round(newValue.doubleValue())));

        // 矩形
        rectangleBorderColorPicker.setOnAction(e -> rectangleConfiguration.setBorderColor(ColorUtils.toHexStr(rectangleBorderColorPicker.getValue())));
        rectangleBorderWidthSlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> rectangleConfiguration.setBorderWidth(Math.round(newValue.doubleValue())));

        // 椭圆
        ellipseBorderColorPicker.setOnAction(e -> ellipseConfiguration.setBorderColor(ColorUtils.toHexStr(ellipseBorderColorPicker.getValue())));
        ellipseBorderWidthSlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> ellipseConfiguration.setBorderWidth(Math.round(newValue.doubleValue())));
    }

    private void initBasicConfiguration() {
        // 是否开机自启
        if (basicConfiguration.getAutoStart()) {
            autoStartBtn.setSelected(true);
        } else {
            notAutoStartBtn.setSelected(true);
        }

        // 0转成10
        int val = (int) (10 - Double.parseDouble((basicConfiguration.getDrawingBoardOpacity() + "").substring(2, 3)));
        drawingBoardOpacitySlider.setValue(val);
    }

    private void initBrushConfiguration() {
        // 是否透明
        if (brushConfiguration.getTransparent()) {
            transparentRadioBtn.setSelected(true);
        } else {
            notTransparentRadioBtn.setSelected(true);
        }
        // 线条颜色
        String strokeColor = brushConfiguration.getStrokeColor();
        brushStrokeColorPicker.setValue(Color.valueOf(strokeColor.equals(GlobalConsts.THEME_COLOR) ? GlobalConsts.TRANSPARENT_COLOR : strokeColor));
        // 线条粗细
        brushLineWidthSlider.setValue(brushConfiguration.getLineWidth());
    }

    private void initRectangleConfiguration() {
        String borderColor = rectangleConfiguration.getBorderColor();
        rectangleBorderColorPicker.setValue(Color.valueOf(borderColor.equals(GlobalConsts.THEME_COLOR) ? GlobalConsts.TRANSPARENT_COLOR : borderColor));
        rectangleBorderWidthSlider.setValue(rectangleConfiguration.getBorderWidth());
    }

    private void initEllipseConfiguration() {
        String borderColor = ellipseConfiguration.getBorderColor();
        ellipseBorderColorPicker.setValue(Color.valueOf(borderColor.equals(GlobalConsts.THEME_COLOR) ? GlobalConsts.TRANSPARENT_COLOR : borderColor));
        ellipseBorderWidthSlider.setValue(ellipseConfiguration.getBorderWidth());
    }

    private void initKeyboardConfiguration() {
        brushKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.BRUSH));
        rectangleKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.RECTANGLE));
        ellipseKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.ELLIPSE));
        toolsKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.TOOLS));
        colorPickerKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.COLOR_PICKER));
        expendMouseKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.EXPEND_MOUSE));
        clearKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.CLEAR));
        screenshotAndCopyKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.SCREENSHOT_AND_COPY));
        openOrCloseKeyboard.setText(keyboardConfiguration.get(KeyBoardConsts.OPEN_OR_CLOSE));
    }
}
