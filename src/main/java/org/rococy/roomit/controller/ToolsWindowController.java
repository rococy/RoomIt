package org.rococy.roomit.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.controller.context.ControllerContext;
import org.rococy.roomit.domain.KeyBoardConfiguration;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Rococy
 * @date 2022/10/6
 */
public class ToolsWindowController implements Initializable {

    @FXML
    private TilePane container;

    @FXML
    private VBox brushImageWrapper;

    @FXML
    private VBox rectangleImageWrapper;

    @FXML
    private VBox ellipseImageWrapper;

    @FXML
    private VBox colorPickerImageWrapper;

    @FXML
    private VBox expendMouseImageWrapper;

    @FXML
    private VBox clearImageWrapper;

    @FXML
    private VBox screenshotAndCopyImageWrapper;

    @FXML
    private VBox openOrCloseImageWrapper;

    private final KeyBoardConfiguration keyBoardConfiguration = ConfigurationManager.getKeyboardConfiguration();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Tooltip.install(brushImageWrapper, new Tooltip("画笔"));
        Tooltip.install(rectangleImageWrapper, new Tooltip("矩形"));
        Tooltip.install(ellipseImageWrapper, new Tooltip("椭圆"));
        Tooltip.install(colorPickerImageWrapper, new Tooltip("颜色选择器"));
        Tooltip.install(expendMouseImageWrapper, new Tooltip("鼠标放大"));
        Tooltip.install(clearImageWrapper, new Tooltip("清空画板"));
        Tooltip.install(screenshotAndCopyImageWrapper, new Tooltip("截屏并复制"));
        Tooltip.install(openOrCloseImageWrapper, new Tooltip("打开或关闭画板"));
    }

    @FXML
    public void vBoxMouseClicked(MouseEvent event) {
        String keyboardType = (String) ((VBox) event.getSource()).getUserData();
        String keyBoardText = keyBoardConfiguration.getKeyBoardMap().get(keyboardType);
        MainWindowController mainWindowController = this.getMainController();
        mainWindowController.triggerKeyboard(keyBoardText);

        // 隐藏当前窗口
        container.getScene().getWindow().hide();
    }

    private MainWindowController getMainController() {
        return (MainWindowController) ControllerContext.getControllerByType(MainWindowController.class);
    }
}
