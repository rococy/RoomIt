package org.rococy.roomit.controller.context;

import org.rococy.roomit.controller.base.BaseController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Rococy
 * @date 2022/10/7
 */
public class ControllerContext {

    private static final Map<String, BaseController> CONTROLLER_MAP = new HashMap<>(4);

    public static void addController(BaseController baseController) {
        CONTROLLER_MAP.put(baseController.getClass().getSimpleName(), baseController);
    }

    public static BaseController getControllerByType(Class<?> type) {
        for (BaseController controller : CONTROLLER_MAP.values()) {
            if (controller.getClass() == type) {
                return controller;
            }
        }
        return null;
    }
}
