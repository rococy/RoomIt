package org.rococy.roomit.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.domain.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rococy
 * @date 2022/9/29
 */
@SuppressWarnings("unchecked")
public class ConfigurationManager {

    private static ShapeConfiguration shapeConfiguration;
    private static KeyBoardConfiguration keyBoardConfiguration;
    private static final Path CONFIGURATION_FILE_PATH = Path.of(GlobalConsts.CONFIGURATION_FILE_PATH);

    static {
        try {
            initConfiguration(Files.readString(CONFIGURATION_FILE_PATH));
            addConfigShutHook();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BasicConfiguration getBasicConfiguration() {
        return shapeConfiguration.getBasic();
    }

    public static BrushConfiguration getBrushConfiguration() {
        return shapeConfiguration.getBrush();
    }

    public static RectangleConfiguration getRectangleConfiguration() {
        return shapeConfiguration.getRectangle();
    }

    public static EllipseConfiguration getEllipseConfiguration() {
        return shapeConfiguration.getEllipse();
    }

    public static KeyBoardConfiguration getKeyboardConfiguration() {
        return keyBoardConfiguration;
    }

    /**
     * 解析配置文件的Json字符串
     *
     * @param jsonStr String
     */
    private static void initConfiguration(String jsonStr) {
        Gson gson = new Gson();
        JsonObject rootObject = JsonParser.parseString(jsonStr).getAsJsonObject();
        JsonObject shapeObject = rootObject.get("shape").getAsJsonObject();

        // 形状配置
        shapeConfiguration = new ShapeConfiguration();
        BasicConfiguration basicConfiguration = gson.fromJson(shapeObject.get("basic"), BasicConfiguration.class);
        BrushConfiguration brushConfiguration = gson.fromJson(shapeObject.get("brush"), BrushConfiguration.class);
        RectangleConfiguration rectangleConfiguration = gson.fromJson(shapeObject.get("rectangle"), RectangleConfiguration.class);
        EllipseConfiguration ellipseConfiguration = gson.fromJson(shapeObject.get("ellipse"), EllipseConfiguration.class);

        shapeConfiguration.setBasic(basicConfiguration);
        shapeConfiguration.setBrush(brushConfiguration);
        shapeConfiguration.setRectangle(rectangleConfiguration);
        shapeConfiguration.setEllipse(ellipseConfiguration);

        // 快捷键配置
        keyBoardConfiguration = new KeyBoardConfiguration(gson.fromJson(rootObject.get("keyboard"), Map.class));
    }

    /**
     * 程序退出时，更新配置文件
     */
    private static void addConfigShutHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Map<String, Object> configMap = new HashMap<>(2);
            configMap.put("shape", shapeConfiguration);
            configMap.put("keyboard", keyBoardConfiguration.getKeyBoardMap());

            try {
                Files.writeString(CONFIGURATION_FILE_PATH, gson.toJson(configMap));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }
}
