package org.rococy.roomit;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.util.ResourceUtils;
import org.rococy.roomit.util.ScreenUtils;

import java.io.IOException;

/**
 * @author Rococy
 */
public class MainWindow {

    private static Stage stage;

    /**
     * 显示或关闭窗口
     */
    public static void toggleDisplay() {
        if (isShowing()) {
            stage.close();
            System.gc();
        } else {
            launch();
        }
    }

    /**
     * @return 窗口是否显示中
     */
    public static boolean isShowing() {
        return stage != null && stage.isShowing();
    }

    /**
     * 启动窗口
     */
    private static void launch() {
        try {
            // 创建新窗口
            stage = new Stage();
            // 加载FXML文件
            FXMLLoader fxmlLoader = new FXMLLoader(ResourceUtils.getResource("fxml/mainWindow.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight());
            // 场景背景透明
            scene.setFill(Paint.valueOf(GlobalConsts.TRANSPARENT_COLOR));

            // 设置场景
            stage.setScene(scene);
            // 设置全屏
            stage.setFullScreen(true);
            // 设置退出全屏的提示文字
            stage.setFullScreenExitHint("");
            // 置顶
            stage.setAlwaysOnTop(true);
            // 禁止缩小
            stage.setResizable(false);
            // 设置窗口透明
            stage.initStyle(StageStyle.TRANSPARENT);
            // 设置图标
            stage.getIcons().add(new Image(ResourceUtils.getResourceAsStream(GlobalConsts.LOGO_PATH)));
            // 将窗口显示出来
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}