package org.rococy.roomit.event;

import javafx.event.Event;
import javafx.event.EventType;

/**
 * @author Rococy
 * @date 2022/9/22
 */
public class RoomItCursorEvent extends Event {

    public static final EventType<RoomItCursorEvent> HIDE = new EventType<>(Event.ANY, "HIDE");
    public static final EventType<RoomItCursorEvent> SHOW = new EventType<>(Event.ANY, "SHOW");

    public RoomItCursorEvent(EventType<? extends Event> eventType) {
        super(eventType);
    }
}
