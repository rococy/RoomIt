package org.rococy.roomit.adapter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Rococy
 * @date 2022/10/17
 */
public abstract class MouseListenerAdapter implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
