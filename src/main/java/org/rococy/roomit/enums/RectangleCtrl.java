package org.rococy.roomit.enums;

/**
 * Shape的八个调整大小的点位
 *
 * @author Rococy
 * @date 2022/9/16
 */
public enum RectangleCtrl {

    /**
     * 上左缩放
     */
    TOP_LEFT,

    /**
     * 上中缩放
     */
    TOP_MIDDLE,

    /**
     * 上右缩放
     */
    TOP_RIGHT,

    /**
     * 中左缩放
     */
    CENTER_LEFT,

    /**
     * 中右缩放
     */
    CENTER_RIGHT,

    /**
     * 下左缩放
     */
    BOTTOM_LEFT,

    /**
     * 下中缩放
     */
    BOTTOM_MIDDLE,

    /**
     * 下右缩放
     */
    BOTTOM_RIGHT,

    /**
     * 移动
     */
    MOVE

}
