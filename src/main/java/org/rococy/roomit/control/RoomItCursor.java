package org.rococy.roomit.control;

import javafx.scene.shape.SVGPath;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.symbol.BrushSymbol;
import org.rococy.roomit.symbol.RectangleSymbol;
import org.rococy.roomit.symbol.base.MouseSymbol;

import java.util.HashMap;
import java.util.Map;

/**
 * 自定义鼠标
 *
 * @author Rococy
 * @date 2022/9/21
 */
public class RoomItCursor extends SVGPath {

    /**
     * 存储鼠标尺寸的对应关系
     */
    private final Map<Integer, Size> sizeLevelMap = new HashMap<>();

    /**
     * 当前尺寸的等级
     */
    private int sizeLevel = 0;

    /**
     * 跟随鼠标的logo
     */
    private MouseSymbol mouseSymbol;

    /**
     * 记录鼠标全局的xy轴位置
     */
    private double x, y;

    public RoomItCursor() {
        // 一开始先隐藏
        super.relocate(-1000, -1000);
        // 绘制路径
        this.setContent("M285 611 c-23 -10 -53 -32 -67 -49 -44 -53 -51 -90 -46 -262 3 -130 7 -162 22 -185 15 -23 22 -26 49 -21 78 16 265 170 306 252 82 164 -95 342 -264 265z");
        // 置顶
        this.setViewOrder(-1);

        // 缩放 和 旋转
        this.setScaleX(0.04);
        this.setScaleY(0.04);
        this.setRotate(-15);

        // 设置渐变色
        this.setStyle("-fx-fill: " + GlobalConsts.THEME_COLOR);

        // small -> 0
        LogoPosition smallLogo = new LogoPosition(28, 8, 27, 10, 32, 13);
        // medium -> 1
        LogoPosition mediumLogo = new LogoPosition(62, 30, 60, 32, 64, 37);
        // large -> 2
        LogoPosition largeLogo = new LogoPosition(170, 80, 168, 82, 172, 87);

        sizeLevelMap.put(0, new Size(this.getScaleX(), this.getRotate(), 187, 259, smallLogo));
        sizeLevelMap.put(1, new Size(0.1, -22, 170, 248, mediumLogo));
        sizeLevelMap.put(2, new Size(0.3, -28, 110, 215, largeLogo));
    }

    /**
     * 扩大
     *
     * @param x x轴坐标
     * @param y y轴坐标
     */
    public void expand(double x, double y) {
        sizeLevel = ++sizeLevel % 3;
        relocate(x, y);
    }

    @Override
    public void relocate(double x, double y) {
        // 记录鼠标的xy轴位置
        this.x = x;
        this.y = y;

        Size size = sizeLevelMap.get(sizeLevel);

        this.setScaleX(size.scale);
        this.setScaleY(size.scale);
        this.setRotate(size.rotate);

        super.relocate(x - size.extraX, y - size.extraY);

        setCursorLogoPosition(x, y, size);
    }

    public void hide() {
        this.setOpacity(0);
        this.mouseSymbol.fillColor(GlobalConsts.TRANSPARENT_COLOR);
    }

    public void setCursorLogo(MouseSymbol mouseSymbol) {
        this.mouseSymbol = mouseSymbol;
        this.initCursorColor();
        this.setCursorLogoPosition(this.x, this.y, sizeLevelMap.get(sizeLevel));
    }

    private void initCursorColor() {
        // 设置logo颜色
        if (this.mouseSymbol instanceof BrushSymbol) {
            this.mouseSymbol.fillColor(ConfigurationManager.getBrushConfiguration().getStrokeColor());
        } else if (this.mouseSymbol instanceof RectangleSymbol) {
            this.mouseSymbol.fillColor(ConfigurationManager.getRectangleConfiguration().getBorderColor());
        } else {
            this.mouseSymbol.fillColor(ConfigurationManager.getEllipseConfiguration().getBorderColor());
        }
    }

    private void setCursorLogoPosition(double x, double y, Size size) {
        // 设置logo位置
        if (this.mouseSymbol instanceof BrushSymbol) {
            this.mouseSymbol.position(x + size.logoPosition.bx, y + size.logoPosition.by);
        } else if (this.mouseSymbol instanceof RectangleSymbol) {
            this.mouseSymbol.position(x + size.logoPosition.rx, y + size.logoPosition.ry);
        } else {
            this.mouseSymbol.position(x + size.logoPosition.ex, y + size.logoPosition.ey);
        }
    }

    private record Size(double scale, double rotate, double extraX, double extraY,
                        LogoPosition logoPosition) {
    }

    private record LogoPosition(double bx, double by, double rx, double ry, double ex, double ey) {
    }
}
