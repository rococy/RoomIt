package org.rococy.roomit.control;

import javafx.event.EventTarget;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import org.rococy.roomit.util.ScreenUtils;

/**
 * 操作画矩形、圆形的面板
 *
 * @author Rococy
 * @date 2022/9/17
 */
public class ShapePane extends Pane {

    /**
     * 矩形
     */
    private ResizableRectangle rectangle;
    private String rectangleBorderColor;

    /**
     * 椭圆
     */
    private ResizableEllipse resizableEllipse;
    private String ellipseBorderColor;

    /**
     * 记录画矩形、圆形的初始x，y轴
     */
    private double startX, startY;

    /**
     * 形状层叠顺序 小的在前面
     */
    private int childViewOrder = Integer.MAX_VALUE;

    public ShapePane() {
        this.setPrefSize(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight());
    }

    /**
     * 设置矩形边框颜色
     *
     * @param hexColor 16进制颜色值
     */
    public void setRectangleBorderColor(String hexColor) {
        this.rectangleBorderColor = hexColor;
    }

    /**
     * 设置椭圆边框颜色
     *
     * @param hexColor 16进制颜色值
     */
    public void setEllipseBorderColor(String hexColor) {
        this.ellipseBorderColor = hexColor;
    }

    /**
     * 画矩形
     */
    public void switchToDrawRectangle() {
        this.setOnMousePressed(e -> {
            // 阻止子元素的事件泡冒上来
            EventTarget eventTarget = e.getTarget();

            if (eventTarget != this || ((Node) eventTarget).isFocusTraversable()) {
                return;
            }

            // 记录起始x、y点
            startX = e.getScreenX();
            startY = e.getScreenY();

            // 创建矩形并加入画板
            rectangle = new ResizableRectangle(startX, startY);


            // 设置用户通过颜色板修改的颜色
            if (this.rectangleBorderColor != null) {
                rectangle.setBorderColor(this.rectangleBorderColor);
            }

            this.addChildren(rectangle);

            // 设置矩形原点在屏幕的位置
            rectangle.setLayoutX(startX);
            rectangle.setLayoutY(startY);
        });

        this.setOnMouseDragged(e -> {
            // 阻止子元素的事件泡冒上来
            EventTarget eventTarget = e.getTarget();
            if (eventTarget != this || ((Node) eventTarget).isFocusTraversable()) {
                return;
            }

            checkRegionBounds(rectangle, startX, startY, e.getScreenX(), e.getScreenY());
        });
    }

    /**
     * 画椭圆
     */
    public void switchToDrawEllipse() {
        this.setOnMousePressed(e -> {
            // 阻止子元素的事件泡冒上来
            EventTarget eventTarget = e.getTarget();
            if (eventTarget != this || ((Node) eventTarget).isFocusTraversable()) {
                return;
            }

            // 获取起始x、y点
            startX = e.getScreenX();
            startY = e.getScreenY();

            // 创建椭圆
            resizableEllipse = new ResizableEllipse(startX, startY);

            // 设置用户通过颜色板修改的颜色
            if (this.ellipseBorderColor != null) {
                resizableEllipse.setBorderColor(this.ellipseBorderColor);
            }

            // 设置包装器原点在屏幕的位置
            resizableEllipse.setLayoutX(startX);
            resizableEllipse.setLayoutY(startY);
            // 放入画板
            this.addChildren(resizableEllipse);
        });

        this.setOnMouseDragged(e -> {
            // 阻止子元素的事件泡冒上来
            EventTarget eventTarget = e.getTarget();
            if (eventTarget != this || ((Node) eventTarget).isFocusTraversable()) {
                return;
            }

            // 检测边界
            this.checkRegionBounds(resizableEllipse, startX, startY, e.getScreenX(), e.getScreenY());

            // 计算椭圆圆心点
            double centerX = resizableEllipse.getPrefWidth() / 2;
            double centerY = resizableEllipse.getPrefHeight() / 2;

            // 椭圆圆心点
            resizableEllipse.setCenterXY(centerX, centerY);
            // 椭圆x、y轴半径
            resizableEllipse.setRadiusXY(centerX - resizableEllipse.getIntervals(), centerY - resizableEllipse.getIntervals());
        });

    }

    public void addChildren(Node node) {
        node.setViewOrder(this.childViewOrder--);
        this.getChildren().add(node);
    }

    public void clear() {
        this.getChildren().clear();
    }

    /**
     * 检测边界
     *
     * @param region region对象
     * @param startX 初始x
     * @param startY 初始y
     * @param endX   末端x
     * @param endY   末端y
     */
    private void checkRegionBounds(Region region, double startX, double startY, double endX, double endY) {
        // 向左上角方向拉伸检测
        if (endX < startX && endY < startY) {
            region.setLayoutX(endX);
            region.setLayoutY(endY);
            region.setPrefSize(startX - endX, startY - endY);
        } else if (endX < startX) {
            // 直直往左拉伸检测
            region.setLayoutX(endX);
            region.setPrefSize(startX - endX, endY - startY);
        } else if (endY < startY) {
            // 直直往上拉伸检测
            region.setLayoutY(endY);
            region.setPrefSize(endX - startX, startY - endY);
        } else {
            // 正常拉伸矩形
            region.setPrefSize(endX - startX, endY - startY);
        }
    }
}
