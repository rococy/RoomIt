package org.rococy.roomit.control;

import javafx.scene.Group;
import javafx.scene.Node;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.domain.BrushConfiguration;

/**
 * 存放canvas的容器
 *
 * @author Rococy
 * @date 2022/9/17
 */
public class CanvasGroup extends Group {

    /**
     * canvas配置
     */
    private static final int TRANSPARENT_CANVAS_COUNT = 5;

    /**
     * 配置项
     */
    private final BrushConfiguration configuration = ConfigurationManager.getBrushConfiguration();
    private final boolean transparent = configuration.getTransparent();

    public CanvasGroup() {
        initCanvas();
    }

    /**
     * 添加子元素
     *
     * @param node Node子元素
     */
    public void addChildren(Node node) {
        this.getChildren().add(node);
    }

    /**
     * 清空画板
     */
    public void clear() {
        if (transparent) {
            return;
        }
        ((RoomItCanvas) this.getChildren().get(0)).clear();
    }

    /**
     * 回退操作
     */
    public void undo() {
        if (transparent) {
            return;
        }
        ((RoomItCanvas) this.getChildren().get(0)).undo();
    }

    /**
     * 初始化Canvas层叠表
     */
    private void initCanvas() {
        if (transparent) {
            // 初始化层叠Canvas
            for (int i = 0; i < TRANSPARENT_CANVAS_COUNT; i++) {
                RoomItCanvas canvas = new RoomItCanvas(true);
                canvas.setViewOrder(i);
                this.addChildren(canvas);
            }
        } else {
            // 直接给一个Canvas即可
            this.addChildren(new RoomItCanvas(false));
        }
    }

}
