package org.rococy.roomit.control;

import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.domain.BrushConfiguration;
import org.rococy.roomit.domain.CanvasPoint;
import org.rococy.roomit.util.ScreenUtils;

import java.util.*;

/**
 * 自动透明的Canvas
 *
 * @author Rococy
 * @date 2022/9/13
 */
public class RoomItCanvas extends Canvas {

    /**
     * 配置项
     */
    private final BrushConfiguration configuration = ConfigurationManager.getBrushConfiguration();

    /**
     * 是否透明
     */
    private final boolean transparent;

    /**
     * 透明度初始值为1
     */
    private double opacity = 1;

    /**
     * 是否可用（是否可以切换到顶层）
     */
    private boolean used = true;

    /**
     * 画笔
     */
    private GraphicsContext gc;

    /**
     * 记录画笔开始画的位置
     */
    private double graphicStartX, graphicStartY;

    private final Timer timer = new Timer();
    private List<List<CanvasPoint>> undoList;
    private List<CanvasPoint> curPointList;

    public RoomItCanvas(boolean transparent) {
        // 初始化宽高
        super(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight());

        this.transparent = transparent;
        if (!transparent) {
            undoList = new ArrayList<>(10);
        }

        // 初始化画笔
        initGraphics();
        // 初始化事件
        initEvents();
    }

    /**
     * 开始慢慢变透明
     */
    public void startTransparency() {
        used = false;
        // 从计时器的任务队列中删除所有取消的任务
        timer.purge();
        timer.schedule(new OpacityTask(), 0, 100);
    }

    /**
     * 返回是否可用（透明度是否已经从0归1）
     *
     * @return 是否可用
     */
    public boolean isUsed() {
        return used;
    }

    /**
     * 设置画笔颜色
     *
     * @param strokeColor 16进制颜色值
     */
    public void setStrokeColor(String strokeColor) {
        configuration.setStrokeColor(strokeColor);
        gc.setStroke(Paint.valueOf(strokeColor));
    }

    /**
     * 清空画板
     */
    public void clear() {
        gc.clearRect(0, 0, this.getWidth(), this.getHeight());
    }

    /**
     * 回退操作
     */
    public void undo() {
        if (undoList == null || undoList.isEmpty()) {
            return;
        }

        String curStrokeColor = configuration.getStrokeColor();

        this.clear();
        // 删除最后一个
        undoList.remove(undoList.size() - 1);
        undoList.forEach(pl ->
                pl.forEach(p -> {
                    this.setStrokeColor(p.strokeColor);
                    gc.strokeLine(p.startX, p.startY, p.endX, p.endY);
                })
        );

        // 还原画笔颜色
        this.setStrokeColor(curStrokeColor);
    }

    /**
     * 初始化画笔
     */
    private void initGraphics() {
        gc = this.getGraphicsContext2D();
        // 画笔颜色
        String strokeColor = configuration.getStrokeColor();
        gc.setStroke(Paint.valueOf(strokeColor.equals(GlobalConsts.THEME_COLOR) ? Color.RED.toString() : strokeColor));
        // 线条宽度
        gc.setLineWidth(configuration.getLineWidth());
        // 让线变得圆滑
        gc.setLineCap(StrokeLineCap.ROUND);
        gc.setLineJoin(StrokeLineJoin.ROUND);
    }

    /**
     * 初始化事件
     */
    private void initEvents() {
        this.bindMousePressedEvent();
        this.bindMouseDraggedEvent();

        if (transparent) {
            this.bindMouseReleasedEvent();
        }
    }

    /**
     * 鼠标按下
     */
    private void bindMousePressedEvent() {
        this.setOnMousePressed(e -> {
            graphicStartX = e.getScreenX();
            graphicStartY = e.getScreenY();

            if (!transparent) {
                curPointList = new ArrayList<>();
                undoList.add(curPointList);
            }
        });
    }

    /**
     * 鼠标拖拽
     */
    private void bindMouseDraggedEvent() {
        // 鼠标按下并拖动
        this.setOnMouseDragged(e -> {
            double endX = e.getX();
            double endY = e.getY();

            // 画线
            gc.strokeLine(graphicStartX, graphicStartY, endX, endY);

            if (!transparent) {
                curPointList.add(new CanvasPoint(graphicStartX, graphicStartY, endX, endY, configuration.getStrokeColor()));
            }

            // 连续画线
            graphicStartX = endX;
            graphicStartY = endY;
        });
    }

    /**
     * 鼠标抬起
     */
    private void bindMouseReleasedEvent() {
        this.setOnMouseReleased(e -> {
            // 当前画完的Canvas开始变透明
            this.startTransparency();

            // 获取相邻的TransparentCanvas
            CanvasGroup group = (CanvasGroup) getParent();
            List<Node> nodeList = group.getChildren();

            for (Node node : nodeList) {
                // 即将置于顶层的canvas
                RoomItCanvas topCanvas = (RoomItCanvas) node;

                // 正在隐藏当中
                if (!topCanvas.isUsed() || topCanvas == this) {
                    continue;
                }

                // 交换图形层叠位置
                this.setViewOrder(topCanvas.getViewOrder());
                topCanvas.setViewOrder(0);
                break;
            }
        });
    }

    /**
     * 透明定时线程
     */
    private class OpacityTask extends TimerTask {

        private final RoomItCanvas canvas = RoomItCanvas.this;

        /**
         * 最小透明度
         */
        private static final double MIN_OPACITY = 0.1;

        @Override
        public void run() {
            opacity *= 0.85;
            canvas.setOpacity(opacity);

            if (opacity < MIN_OPACITY) {
                canvas.clear();
                opacity = 1;
                canvas.setOpacity(opacity);
                used = true;
                this.cancel();
            }
        }
    }
}
