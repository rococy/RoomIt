package org.rococy.roomit.control;

import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.control.base.Resizer;
import org.rococy.roomit.domain.RectangleConfiguration;
import org.rococy.roomit.util.StyleRenderer;

/**
 * 能改变尺寸的矩形
 *
 * @author Rococy
 * @date 2022/9/19
 */
public class ResizableRectangle extends Resizer {

    /**
     * 内部矩形
     */
    private final InnerRectangle innerRectangle;

    /**
     * @param originX 在整个屏幕内的初始x值
     * @param originY 在整个屏幕内的初始y值
     */
    public ResizableRectangle(double originX, double originY) {
        super(originX, originY);
        this.innerRectangle = new InnerRectangle();
        super.addChildren(this.innerRectangle);
        this.bindPrefSizeChangeEvent();
    }

    @Override
    public void setPrefSize(double prefWidth, double prefHeight) {
        int intervals = this.getIntervals() * 2;

        // 设置间隔
        this.innerRectangle.setLayoutX(intervals);
        this.innerRectangle.setLayoutY(intervals);
        this.innerRectangle.setPrefWidth(prefWidth - intervals * 2);
        this.innerRectangle.setPrefHeight(prefHeight - intervals * 2);

        super.setPrefSize(prefWidth, prefHeight);
    }

    @Override
    protected Parent getRootNode() {
        return this.getParent().getParent();
    }

    /**
     * 设置边框颜色
     *
     * @param borderColor 边框颜色 hex
     */
    public void setBorderColor(String borderColor) {
        this.innerRectangle.setBorderColor(borderColor);
    }

    private void bindPrefSizeChangeEvent() {
        int intervals = this.getIntervals() * 2;

        this.prefHeightProperty().addListener((observable, oldValue, newValue) -> {
            this.innerRectangle.setLayoutY(intervals);
            this.innerRectangle.setPrefHeight(((double) newValue) - intervals * 2);
        });

        this.prefWidthProperty().addListener((observable, oldValue, newValue) -> {
            this.innerRectangle.setLayoutX(intervals);
            this.innerRectangle.setPrefWidth(((double) newValue) - intervals * 2);
        });
    }

    private static class InnerRectangle extends Pane {

        /**
         * 最小宽高
         */
        private static final double MIN_WIDTH = 8, MIN_HEIGHT = 8;

        /**
         * 配置项
         */
        private final RectangleConfiguration rectangleConfiguration;

        /**
         * 样式集合
         */
        private final StyleRenderer styleRenderer = new StyleRenderer(this);

        public InnerRectangle() {
            rectangleConfiguration = ConfigurationManager.getRectangleConfiguration();

            // 填充颜色、边框颜色、边框宽度、圆角
            styleRenderer.add("-fx-fill", GlobalConsts.TRANSPARENT_COLOR)
                    .add("-fx-border-color", rectangleConfiguration.getBorderColor())
                    .add("-fx-border-width", rectangleConfiguration.getBorderWidth())
                    .add("-fx-border-style", "solid")
                    .add("-fx-border-radius", 5)
                    .render();

            this.minWidth(MIN_HEIGHT);
            this.minHeight(MIN_WIDTH);
        }

        /**
         * 设置矩形边框颜色
         *
         * @param hexColor 16进制颜色值
         */
        public void setBorderColor(String hexColor) {
            rectangleConfiguration.setBorderColor(hexColor);
            styleRenderer.add("-fx-border-color", hexColor).render();
        }
    }

}
