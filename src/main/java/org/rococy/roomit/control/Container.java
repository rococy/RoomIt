package org.rococy.roomit.control;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * 最外层容器
 *
 * @author Rococy
 * @date 2022/9/24
 */
public class Container extends Pane {

    /**
     * 添加子元素
     *
     * @param nodes Node对象
     */
    public void addChildren(Node... nodes) {
        this.getChildren().addAll(nodes);
    }

    /**
     * 删除子元素
     *
     * @param node Node对象
     */
    public void removeChild(Node node) {
        this.getChildren().remove(node);
    }

}
