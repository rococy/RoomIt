package org.rococy.roomit.control;

import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;

/**
 * 滑动条
 *
 * @author Rococy
 * @date 2022/10/12
 */
public class RoomItSlider extends Slider {


    public RoomItSlider() {
        // 显示刻度值
        this.setShowTickLabels(true);
        // 刻度之间的单位距离
        this.setMajorTickUnit(1);
        this.initEvents();
    }

    private void initEvents() {
        // 使滑动块按步数走
        this.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> this.setValue(Math.round(this.getValue())));
    }

}
