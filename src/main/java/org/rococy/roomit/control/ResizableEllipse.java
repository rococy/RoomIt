package org.rococy.roomit.control;

import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Ellipse;
import org.rococy.roomit.config.ConfigurationManager;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.control.base.Resizer;
import org.rococy.roomit.domain.EllipseConfiguration;
import org.rococy.roomit.util.StyleRenderer;

/**
 * @author Rococy
 * @date 2022/9/18
 */
public class ResizableEllipse extends Resizer {

    /**
     * 内部椭圆
     */
    private final InnerEllipse innerEllipse = new InnerEllipse();

    /**
     * @param originX 在整个屏幕内的初始x值
     * @param originY 在整个屏幕内的初始y值
     */
    public ResizableEllipse(double originX, double originY) {
        super(originX, originY);

        // 添加内椭圆
        this.addChildren(innerEllipse);

        // 初始化事件
        initEvents();
    }

    @Override
    protected Parent getRootNode() {
        return this.getParent().getParent();
    }

    /**
     * 设置边框颜色
     *
     * @param borderColor 边框颜色 hex
     */
    public void setBorderColor(String borderColor) {
        this.innerEllipse.setBorderColor(borderColor);
    }

    /**
     * 设置中心点
     *
     * @param centerX x
     * @param centerY y
     */
    public void setCenterXY(double centerX, double centerY) {
        this.innerEllipse.setCenterX(centerX);
        this.innerEllipse.setCenterY(centerY);
    }

    /**
     * 设置中心点
     *
     * @param radiusX x
     * @param radiusY y
     */
    public void setRadiusXY(double radiusX, double radiusY) {
        this.innerEllipse.setRadiusX(radiusX);
        this.innerEllipse.setRadiusY(radiusY);
    }

    /**
     * 初始化事件
     */
    private void initEvents() {
        // 鼠标拖拽事件
        bindMouseDraggedEvent();
    }

    /**
     * 拓展鼠标拖拽事件
     */
    private void bindMouseDraggedEvent() {
        EventHandler<MouseEvent> mouseDraggedEventHandler = e -> {
            double centerX = this.getPrefWidth() / 2;
            double centerY = this.getPrefHeight() / 2;

            // 椭圆圆心点
            innerEllipse.setCenterX(centerX);
            innerEllipse.setCenterY(centerY);

            // 椭圆x、y轴半径
            innerEllipse.setRadiusX(centerX - this.getIntervals());
            innerEllipse.setRadiusY(centerY - this.getIntervals());
        };

        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedEventHandler);
    }


    /**
     * 矩形内部的椭圆
     */
    public static class InnerEllipse extends Ellipse {

        /**
         * 样式集合
         */
        private final StyleRenderer styleRenderer = new StyleRenderer(this);

        /**
         * 配置项
         */
        private final EllipseConfiguration ellipseConfiguration = ConfigurationManager.getEllipseConfiguration();

        public InnerEllipse() {
            styleRenderer.add("-fx-fill", GlobalConsts.TRANSPARENT_COLOR)
                    .add("-fx-stroke", ellipseConfiguration.getBorderColor())
                    .add("-fx-stroke-width", ellipseConfiguration.getBorderWidth())
                    .render();
        }

        /**
         * 设置椭圆边框颜色
         *
         * @param hexColor 16进制颜色值
         */
        public void setBorderColor(String hexColor) {
            ellipseConfiguration.setBorderColor(hexColor);
            styleRenderer.add("-fx-stroke", hexColor).render();
        }
    }

}
