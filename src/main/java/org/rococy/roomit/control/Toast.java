package org.rococy.roomit.control;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import org.rococy.roomit.function.Handler;
import org.rococy.roomit.thread.ToastThreadFactory;
import org.rococy.roomit.util.ScreenUtils;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 消息提示框，过一定时间慢慢消失（透明度）
 *
 * @author Rococy
 * @date 2022/9/8
 */
public class Toast extends Label {

    /**
     * 定时线程池
     */
    private final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1, new ToastThreadFactory());

    /**
     * 每次透明度减少的间隔毫秒数
     */
    private static final int PERIOD = 30;

    /**
     * 默认一秒后开始慢慢减少透明度
     */
    private static final int DEFAULT_TIMEOUT = 1000;

    /**
     * 初始化透明度
     */
    private double opacity = 1;

    private final Handler startHandler;
    private Handler releaseHandler;

    public Toast(String msg) {
        this(msg, DEFAULT_TIMEOUT, null);
    }

    public Toast(String msg, Handler transparentStartHandler) {
        this(msg, DEFAULT_TIMEOUT, transparentStartHandler);
    }

    public Toast(String msg, int timeout, Handler transparentStartHandler) {
        this.startHandler = transparentStartHandler;

        // 基本样式
        this.setStyle("-fx-text-fill:#fff;-fx-font-size:14px;-fx-font-weight:bold;-fx-padding:15px 35px;-fx-background-color:linear-gradient(to right,#f64f59,#c471ed,#12c2e9);-fx-background-radius:20px;");

        // 设置文本
        this.setText(msg);

        // 慢慢透明
        slowlyTransparent(timeout);
    }

    public void setOnTransparentReleased(Handler handler) {
        this.releaseHandler = handler;
    }

    private void slowlyTransparent(int timeout) {
        // 获取屏幕宽高
        double screenWidth = ScreenUtils.getScreenWidth();
        double screenHeight = ScreenUtils.getScreenHeight();

        // 将Toast位置设置到屏幕下方中央位置
        this.setLayoutX(screenWidth - (screenWidth / 2) - (this.getTextWidth() / 2));
        this.setLayoutY(screenHeight - 150);

        if (this.startHandler != null) {
            this.startHandler.apply();
        }

        // 延迟timeout毫秒后开始虚化提示框
        scheduledExecutorService.schedule(this::hidden, timeout, TimeUnit.MILLISECONDS);
    }

    /**
     * 根据字数来计算标签宽度
     *
     * @return 宽度
     */
    private double getTextWidth() {
        return this.getText().length() * 24;
    }

    /**
     * 慢慢减少透明度
     */
    private void hidden() {
        // 使用定时器实现
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                opacity -= 0.01;
                Toast.this.setOpacity(opacity);

                if (opacity < 0.0) {
                    // 从父容器中删除自身
                    Platform.runLater(() -> ((Pane) Toast.this.getParent()).getChildren().remove(Toast.this));
                    if (Toast.this.releaseHandler != null) {
                        Platform.runLater(() -> Toast.this.releaseHandler.apply());
                    }
                    // 停止定时器
                    timer.cancel();
                }
            }
        }, 0, PERIOD);

    }

}