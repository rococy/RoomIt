package org.rococy.roomit.control.base;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.rococy.roomit.constant.GlobalConsts;
import org.rococy.roomit.enums.RectangleCtrl;
import org.rococy.roomit.event.RoomItCursorEvent;
import org.rococy.roomit.util.StyleRenderer;

/**
 * 能改变尺寸的容器
 *
 * @author Rococy
 * @date 2022/9/14
 */
public abstract class Resizer extends Pane {

    /**
     * 配置项
     */
    private final double borderWidth = 3;

    /**
     * 与内部形状的间隔距离
     */
    private int intervals = 3;

    /**
     * 样式集合
     */
    private final StyleRenderer styleRenderer = new StyleRenderer(this);

    /**
     * 矩形原点
     */
    private double originX, originY;

    /**
     * 记录调整完矩形后的大小
     */
    private double width, height;

    /**
     * 当处于移动状态时，记录的xy值
     */
    private double moveX, moveY;

    /**
     * 矩形操作类型
     */
    private RectangleCtrl rectangleCtrl;

    /**
     * @param originX 在整个屏幕内的初始x值
     * @param originY 在整个屏幕内的初始y值
     */
    public Resizer(double originX, double originY) {
        // 记录原点
        this.originX = originX;
        this.originY = originY;

        // 初始化样式
        this.initStyles();
        // 初始化调整大小事件
        this.initEvents();
    }

    /**
     * 获取最外层的父级节点
     *
     * @return 最外层的父级节点
     */
    protected abstract Parent getRootNode();

    /**
     * 设置边框样式
     */
    public void setBorderWidth(double borderWidth) {
        styleRenderer.add("-fx-border-width", borderWidth).render();
    }

    /**
     * 获取内部间隔
     */
    public int getIntervals() {
        return (int) (this.borderWidth + this.intervals);
    }

    /**
     * 添加子元素
     *
     * @param node Node元素
     */
    protected void addChildren(Node node) {
        this.getChildren().add(node);
    }

    /**
     * 初始化样式
     */
    private void initStyles() {
        // 设置样式
        styleRenderer.add("-fx-border-width", borderWidth)
                .add("-fx-border-style", "dashed")
                .add("-fx-border-color", GlobalConsts.THEME_COLOR)
                .add("-fx-border-radius", 1)
                .render();
    }

    /**
     * 将默认的鼠标隐藏
     */
    private void hideRoomItCursor() {
        this.getRootNode().fireEvent(new RoomItCursorEvent(RoomItCursorEvent.HIDE));
    }

    /**
     * 将默认的鼠标显示出来
     */
    private void showRoomItCursor() {
        this.getRootNode().fireEvent(new RoomItCursorEvent(RoomItCursorEvent.SHOW));
    }

    /**
     * 初始化矩形调整大小事件
     */
    private void initEvents() {
        this.bindMouseEnterEvent();
        this.bindMouseExitedEvent();
        this.bindMousePressedEvent();
        this.bindMouseMovedEvent();
        this.bindMouseDraggedEvent();
        this.bindMouseReleasedEvent();

        this.bindKeyPressedEvent();

        this.bindFocusTraversableChangeListener();
    }

    /**
     * 鼠标进入
     */
    private void bindMouseEnterEvent() {
        this.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> {
            this.requestFocus();
            this.setFocusTraversable(true);
            this.hideRoomItCursor();
        });
    }

    /**
     * 鼠标退出
     */
    private void bindMouseExitedEvent() {
        this.addEventHandler(MouseEvent.MOUSE_EXITED, e -> {
            this.showRoomItCursor();
            this.setFocusTraversable(false);
        });
    }

    /**
     * 鼠标按下
     */
    private void bindMousePressedEvent() {
        this.addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
            width = this.getWidth();
            height = this.getHeight();

            if (rectangleCtrl == RectangleCtrl.MOVE) {
                moveX = e.getScreenX() - this.getLayoutX();
                moveY = e.getScreenY() - this.getLayoutY();
            }
        });
    }

    /**
     * 鼠标移动
     */
    private void bindMouseMovedEvent() {
        this.addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
            double width = this.getWidth(),
                    height = this.getHeight(),
                    x = e.getScreenX() - this.getLayoutX(),
                    y = e.getScreenY() - this.getLayoutY();


            // topLeft
            if (x <= borderWidth && y <= borderWidth) {
                this.setCursor(Cursor.SE_RESIZE);
                rectangleCtrl = RectangleCtrl.TOP_LEFT;
                return;
            }

            // topMiddle
            if (x <= width - borderWidth && y <= borderWidth) {
                this.setCursor(Cursor.N_RESIZE);
                rectangleCtrl = RectangleCtrl.TOP_MIDDLE;
                return;
            }

            // topRight
            if (x <= width && y <= borderWidth) {
                this.setCursor(Cursor.NE_RESIZE);
                rectangleCtrl = RectangleCtrl.TOP_RIGHT;
                return;
            }

            // centerLeft
            if (x <= borderWidth && y <= height - borderWidth) {
                this.setCursor(Cursor.W_RESIZE);
                rectangleCtrl = RectangleCtrl.CENTER_LEFT;
                return;
            }

            // bottomLeft
            if (x <= borderWidth && y <= height) {
                this.setCursor(Cursor.NE_RESIZE);
                rectangleCtrl = RectangleCtrl.BOTTOM_LEFT;
                return;
            }

            // bottomMiddle
            if (x <= width - borderWidth && y >= height - borderWidth) {
                this.setCursor(Cursor.N_RESIZE);
                rectangleCtrl = RectangleCtrl.BOTTOM_MIDDLE;
                return;
            }

            // bottomRight
            if (x <= width && y >= height - borderWidth) {
                this.setCursor(Cursor.SE_RESIZE);
                rectangleCtrl = RectangleCtrl.BOTTOM_RIGHT;
                return;
            }

            // centerRight
            if (x >= width - borderWidth && y <= height) {
                this.setCursor(Cursor.W_RESIZE);
                rectangleCtrl = RectangleCtrl.CENTER_RIGHT;
                return;
            }

            // 移动指针
            if (x >= borderWidth + 1 && x <= width - borderWidth && y >= borderWidth + 1 && y <= height - borderWidth) {
                this.setCursor(Cursor.MOVE);
                rectangleCtrl = RectangleCtrl.MOVE;
                this.setFocusTraversable(true);
                return;
            }

            // 特殊情况
            this.setCursor(Cursor.DEFAULT);
        });
    }

    /**
     * 鼠标拖拽
     */
    private void bindMouseDraggedEvent() {
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
            // 防止快速操作
            if (rectangleCtrl == null) {
                return;
            }

            this.hideRoomItCursor();

            double screenX = e.getScreenX(),
                    screenY = e.getScreenY(),
                    x = screenX - originX,
                    y = screenY - originY;

            switch (rectangleCtrl) {
                case TOP_LEFT -> {
                    this.setLayoutX(screenX);
                    this.setLayoutY(screenY);
                    this.setPrefSize(width - x, height - y);
                }

                case TOP_MIDDLE -> {
                    this.setLayoutY(screenY);
                    this.setPrefHeight(height - y);
                }

                case TOP_RIGHT -> {
                    this.setLayoutY(screenY);
                    this.setPrefSize(x, height - y);
                }

                case CENTER_LEFT -> {
                    this.setLayoutX(screenX);
                    this.setPrefWidth(width - x);
                }

                case CENTER_RIGHT -> this.setPrefWidth(x);

                case BOTTOM_LEFT -> {
                    this.setLayoutX(screenX);
                    this.setPrefSize(width - x, y);
                }

                case BOTTOM_MIDDLE -> this.setPrefHeight(y);

                case BOTTOM_RIGHT -> this.setPrefSize(x, y);

                case MOVE -> {
                    this.setLayoutX(screenX - moveX);
                    this.setLayoutY(screenY - moveY);
                }
            }
        });
    }

    /**
     * 鼠标抬起
     */
    private void bindMouseReleasedEvent() {
        this.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
            this.originX = this.getLayoutX();
            this.originY = this.getLayoutY();
        });
    }

    /**
     * 监听键盘按下
     */
    private void bindKeyPressedEvent() {
        this.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            switch (e.getCode()) {
                case DELETE -> {
                    // 删除自身
                    Pane parent = (Pane) this.getParent();
                    parent.getChildren().remove(this);
                }
            }
        });
    }

    /**
     * 聚焦状态变化事件
     */
    private void bindFocusTraversableChangeListener() {
        this.focusTraversableProperty().addListener((observable, oldValue, focused) -> {
            this.setBorderWidth(focused ? this.borderWidth : 0);
        });
    }
}
