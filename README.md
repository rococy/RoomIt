![输入图片说明](img/full-logo.png)
# 介绍
<p>RoomIt是一款基于JavaFx、操作简便、轻量的屏幕画笔工具。</p>
<p>RoomIt名字组成：Rococy + ZoomIt。</p>
<p>RoomIt开发初衷：提升技术水平、为开源社区贡献一份力。</p>

# 演示
![无法加载gif演示图，请刷新页面](img/demo.gif)
<br/>

# 文档
<p>用户文档: <a href="https://gitee.com/gavaFullStack/RoomIt/wikis/UserManual">https://gitee.com/gavaFullStack/RoomIt/wikis/UserManual</a></p>
<p>开发文档：<a href="https://gitee.com/gavaFullStack/RoomIt/wikis/Development">https://gitee.com/gavaFullStack/RoomIt/wikis/Development</a> </p>
<p>版本更新文档：<a href="https://gitee.com/gavaFullStack/RoomIt/wikis/Versions">https://gitee.com/gavaFullStack/RoomIt/wikis/Versions</a></p>

# 下载
<p>Windows版本：<a href="https://gitee.com/gavaFullStack/RoomIt/raw/master/dist/RoomIt-win-x64.zip">https://gitee.com/gavaFullStack/RoomIt/raw/master/dist/RoomIt-win-x64.zip</a></p>
<p>Mac版本：暂无。</p>
<p>Linux版本：暂无。</p>

# 联系方式
邮箱：rococy@qq.com